require 'bundler/capistrano'
require 'capistrano-rbenv'
require 'capistrano-unicorn'

set :application, 'ciudig-hackaton'
set :repository,  'git@git.divux.com:mi-mascota/web.git'

server 'leia.divux.com', :web, :app, :db, primary: true

set :deploy_to, "/srv/apps/saas/#{application}"
set :deploy_via, :remote_cache

set :user, 'deployer'
set :use_sudo, false

set :shared_children, shared_children + %w{public/uploads tmp/sockets}

set :rbenv_ruby_version, '2.0.0-p195'
